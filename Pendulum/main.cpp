#include <iostream>
#include <math.h>
#include "Scalars.h"
#include "CSVWriter.h"

struct Pendulum
{
	double theta;	// in rad
	double mass;	// in kg.
	double weight;	// in N
	double length;	// in m
	double tension; 
} pendulum;

struct OldValues
{
	double theta;
} oldValues;

// Files
CSVWriter* simulationFile = new CSVWriter("PendulumSimulation.csv", ",");

// Time variables
double t = 0, n = 0;
const double timeDuration = 30; // Time Duration
const double h = 0.01; // timestep

// Physics variables
const double damper = 0.5;
double A = 0;
double angularAcc = 0;
double angularVel = 0;
double rotInertia = 0;
double omega = 0;

// Euler integration variables
double newAngularVel = 0, newTheta = 0, theta = 0;

void init(void);
double improvedEulerIntegrationForearm(double dt);
void setSimulationFileHeader(void);
void writeSimulationFileHeader(void);
double degToRad(double deg);
double radToDeg(double rad);

int main(int argc, char** argv)
{
	init();

	while (t < timeDuration)
	{
		oldValues.theta = pendulum.theta;
		// Method 1
		/*angularAcc = (-g / pendulum.length) * sin(pendulum.theta);*/

		// Method 2
		
		angularAcc = (-g / pendulum.length) * sin(pendulum.theta)
			- ((damper / pendulum.mass) * pow(pendulum.length, 2)) * newAngularVel
			+ ((A / pendulum.mass) * pow(pendulum.length, 2)) * cos((2 / 3)*h);
		
		
		// Method 3
		//oldValues.theta = pendulum.theta;
		//pendulum.theta = oldValues.theta * cos(omega * h);
		//pendulum.theta = pendulum.theta * cos(omega * h);

		// Method 4
		//angularAcc = -(g / pendulum.length) * pendulum.theta;
		//angularAcc = -(g / pendulum.length) * sin(pendulum.theta);

		// Method 5
		//oldValues.theta = pendulum.theta;
		//pendulum.theta = 


		/*
		newAngularVel += angularAcc * h;
		newAngularVel *= damper;
		pendulum.theta += newAngularVel*h;
		*/

		
		// Euler Integration
		angularVel = newAngularVel;
		theta = pendulum.theta;

		// New angular velocity
		newAngularVel = angularVel + (angularAcc * h);
		// New theta
		newTheta = theta + (((angularVel + newAngularVel) / 2.0) * h);
		//newTheta = theta + (angularVel * h);
		pendulum.theta = newTheta;
		
		//pendulum.theta = improvedEulerIntegrationForearm(h);
		
		writeSimulationFileHeader();

		n += 1;
		t = n * h;
	}

	return 0;
}

void init(void)
{

	pendulum.length = 0.26; // m
	pendulum.theta = degToRad(45.0); // in rad
	pendulum.mass = 1.5; // kg
	pendulum.weight = pendulum.mass * -g; // acc in rad/s^2

	A = 1.15;
	angularAcc = 0; //rad/s^2
	newTheta = pendulum.theta;
	omega = sqrt(g / pendulum.length);

	setSimulationFileHeader();

}

double improvedEulerIntegrationForearm(double dt)
{
	double w_old = 0.0f, theta_old = 0.0f;

	w_old = newAngularVel;
	theta_old = newTheta;

	// New angular velocity
	newAngularVel = w_old + (angularAcc * dt);
	// New angle
	//newTheta = theta_old + ((newAngularVel + w_old) / 2.0) * dt;

	newTheta = theta_old + (newAngularVel * dt);

	return newTheta; // value in rad
}

void setSimulationFileHeader(void)
{
	simulationFile->AddHeaderColum("Time");
	simulationFile->AddHeaderColum("TimeStep");
	simulationFile->AddHeaderColum("ThetaOld");
	simulationFile->AddHeaderColum("ThetaNew");
	simulationFile->AddHeaderColum("AngularAcc");
	simulationFile->AddHeaderColum("AngularVel");

	simulationFile->WriteHeader();
}

void writeSimulationFileHeader(void)
{
	simulationFile->ClearText();
	simulationFile->AddText(to_string(t));
	simulationFile->AddText(to_string(h));
	simulationFile->AddText(to_string(oldValues.theta));
	simulationFile->AddText(to_string(pendulum.theta));
	simulationFile->AddText(to_string(angularAcc));
	simulationFile->AddText(to_string(newAngularVel));

	simulationFile->WriteTempLine();
}

double degToRad(double deg)
{
	return deg * (PI / 180);
}

double radToDeg(double rad)
{
	return rad * (180.0 / PI);
}