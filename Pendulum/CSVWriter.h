#pragma once

#include <iostream>
#include <vector>
#include <string>

using namespace std;

class CSVWriter
{
private:
	string _fileName;
	string _delimiter;
	int _lineCounter;
	vector<string> _tmpTextLine;
	vector<string> _header;

public:
	CSVWriter(string fileName, string delimiter);
	~CSVWriter();
	void AddHeaderColum(string newColumn);
	void WriteHeader(void);
	void WriteTempLine(void);
	template<typename T>
	void WriteLine(T first, T last);
	void ClearText(void);
	void AddText(string newText);
	vector<string> GetText(void);
};